package com.calculator;

public class Calculator {


    private final IDisplay display;
    private boolean lastButtonWasDigital;

    public String getOperator() {
        return operator;
    }

    private String operator = "+";
    private double a;

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    private double b;

    public Calculator(IDisplay display) {
        this.display = display;
    }

    public double getDigital() {
        return Double.parseDouble(display.getDisplayNumber());
    }

    public void setDigital(double digital) {
        display.setDisplayDigital(String.valueOf(digital));
    }

    public void checkDisplayDigital(String digital) {
        if (lastButtonWasDigital) {
            display.setDisplayDigital(display.getDisplayNumber() + digital);
        } else {
            display.setDisplayDigital(digital);
        }
        lastButtonWasDigital = true;
    }

    public void clearDisplay() {
        display.setDisplayDigital("0");
        lastButtonWasDigital = false;
    }

    public void negate() {
        double newDigital = getDigital() * -1;
        setDigital(newDigital);
    }

    public void comma() {
        if (!display.getDisplayNumber().contains(".")) {
            display.setDisplayDigital(display.getDisplayNumber() + ".");
        }
        lastButtonWasDigital = true;
    }

    public void operator(String operator) {
        if (lastButtonWasDigital) {
            b = getDigital();
            calc();
        }
        this.operator = operator;
    }

    public void enter() {
        if (lastButtonWasDigital) {
            b = getDigital();
        }
        calc();
    }

    public void calc() {

        switch (operator) {
            case "+":
                setDigital(a + b);
                break;
            case "-":
                setDigital(a - b);
                break;
            case "*":
                setDigital(a * b);
                break;
            case "/":
                if (b != 0) {
                    throw new IllegalStateException("division by zero is not possible");
                }
                setDigital(a / b);
                break;
            default:
                throw new IllegalStateException("Unknow operator" + this.operator);
        }
        a = getDigital();
        lastButtonWasDigital = false;

    }

}
