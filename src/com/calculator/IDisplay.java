package com.calculator;

public interface IDisplay {
    String getDisplayNumber();

    void setDisplayDigital(String displayNumber);
}
