package com.calculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;


public class CalcController implements IDisplay {
    @FXML
    private TextField display;


    private Calculator calculator;

    public CalcController() {
        calculator = new Calculator(this);
    }

    @Override
    public void setDisplayDigital(String displayNumber) {
        display.setText(displayNumber);
    }

    @Override
    public String getDisplayNumber() {
        return display.getText();
    }


    public void ClickBackSpaceButtonAction(ActionEvent actionEvent) {
        System.out.println("BackSpace click");
    }

    public void ClickDigitalButtonAction(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String digital = button.getText();
        calculator.checkDisplayDigital(digital);
    }

    public void clickClearButtonAction(ActionEvent actionEvent) {
        calculator.clearDisplay();
    }

    public void clickCommaButtonAction(ActionEvent actionEvent) {
        calculator.comma();
    }

    public void clickNegateButtonAction(ActionEvent actionEvent) {
        calculator.negate();
    }

    public void clickPlusButtonAction(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String operator = button.getText();
        calculator.operator(operator);

    }


}